#See https://aka.ms/containerfastmode to understand how Visual Studio uses this Dockerfile to build your images for faster debugging.

FROM mcr.microsoft.com/dotnet/aspnet:6.0 AS base
WORKDIR /app
EXPOSE 80

FROM mcr.microsoft.com/dotnet/sdk:6.0 AS build
WORKDIR /src
COPY ["src/Explorers.DriveIn.WebHost/Explorers.DriveIn.WebHost.csproj", "Explorers.DriveIn.WebHost/"]
COPY ["src/Explorers.DriveIn.DataAccess/Explorers.DriveIn.DataAccess.csproj", "Explorers.DriveIn.DataAccess/"]
COPY ["src/Explorers.DriveIn.Core/Explorers.DriveIn.Core.csproj", "Explorers.DriveIn.Core/"]
RUN dotnet restore "Explorers.DriveIn.WebHost/Explorers.DriveIn.WebHost.csproj"
COPY . .
WORKDIR "/src/src/Explorers.DriveIn.WebHost"
RUN dotnet build "Explorers.DriveIn.WebHost.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "Explorers.DriveIn.WebHost.csproj" -c Release -o /app/publish

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "Explorers.DriveIn.WebHost.dll"]
