﻿using Explorers.DriveIn.TrainingPointsService.Core.Abstractions;
using Explorers.DriveIn.TrainingPointsService.Core.Abstractions.Repositories;
using Explorers.DriveIn.TrainingPointsService.Core.Domain;
using Explorers.DriveIn.TrainingPointsService.WebHost.Models;
using Microsoft.AspNetCore.Mvc;
using Serilog;
using System.Runtime.CompilerServices;

namespace Explorers.DriveIn.TrainingPointsService.WebHost.Controllers
{
    /// <summary>
    /// Training points
    /// </summary>
    [ApiController]
    [Route("api/v1/points/[controller]")]
    public class TrainingPointsController : Controller
    {
        //Models context
        private readonly IRepository<TrainingPoint> _trainingPointsRepository;
        private readonly ICacheService _cacheService;

        /// <summary>
        /// Training points controller constructor
        /// </summary>
        /// <param name="trainingsRepository"></param>
        /// <param name="trainingPointsRepository"></param>
        public TrainingPointsController(IRepository<TrainingPoint> trainingPointsRepository, 
            ICacheService cacheService)
        {
            _trainingPointsRepository = trainingPointsRepository;
            _cacheService = cacheService;
        }

        /// <summary>
        /// Get all training points
        /// </summary>
        /// <returns>Training points collection</returns>
        [HttpGet]
        //public async Task<ActionResult<IEnumerable<TrainingPoint>>> GetTrainingPointsAsync()
        //{
        //    var cacheKey = "trainingPointsList";

        //    var cached = _cacheService.Get<IEnumerable<TrainingPoint>>(cacheKey);

        //    if(cached != null && cached.Any())
        //    {
        //        return Ok(cached);
        //    }
        //    else
        //    {
        //        var trainingPointsList = await _trainingPointsRepository.GetAllAsync();
        //        return Ok(_cacheService.Set(cacheKey, trainingPointsList));
        //    }           
        //}

        /// <summary>
        /// Get training point by Id  
        /// </summary>
        /// <returns>Training point</returns>
        //[HttpGet("{id:guid}")]
        //public async Task<ActionResult<TrainingPoint>> GetTrainingPointAsync(Guid id)
        //{
        //    var cacheKey = $"trainingPointById:{id}";
        //    var cached = _cacheService.Get<TrainingPoint>(cacheKey);

        //    if (cached != null)
        //    {
        //        return Ok(cached);
        //    }
        //    else
        //    {
        //        var trainingPoint = await _trainingPointsRepository.GetByIdAsync(id);
        //        return Ok(_cacheService.Set(cacheKey, trainingPoint));
        //    }
        //}

        /// <summary>
        /// Get training point by training Id  
        /// </summary>
        /// <param name="trainingId">Training id</param>
        /// <returns>Training points</returns>
        //[Route("by-training-id/{trainingId:guid}")]
        //[HttpGet]
        //public async Task<ActionResult<IEnumerable<TrainingPoint>>> GetTrainingPointsByTrainingId(Guid trainingId)
        //{
        //    var cacheKey = $"trainingPointByTrainingId:{trainingId}";
        //    var cached = _cacheService.Get<TrainingPoint>(cacheKey);

        //    if (cached != null)
        //    {
        //        return Ok(cached);
        //    }
        //    else
        //    {
        //        var allTrainingPoints = await _trainingPointsRepository.GetAllAsync();
        //        var trainingPoints = allTrainingPoints.Where(x => x.TrainingId == trainingId);
        //        return Ok(_cacheService.Set(cacheKey, trainingPoints));
        //    }
        //}

        /// <summary>
        /// Create training point
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> CreateTrainingPointAsync(CreateOrEditPointRequest request)
        {
            TrainingPoint point = new TrainingPoint
            {
                TrainingId = request.TrainingId,
                Latitude = request.Latitude,
                Longitude = request.Longitude,
                DatePoint = request.DatePoint
            };

            await _trainingPointsRepository.AddAsync(point);

            var trainingPointAsyncName = "";//nameof(GetTrainingPointAsync);
            return CreatedAtAction(trainingPointAsyncName, new { id = point.Id }, null);
        }

        /// <summary>
        /// Update training point
        /// </summary>
        /// <param name="id">Training point id</param>
        /// <param name="request">Update request</param>
        /// <returns></returns>
        [HttpPut("{id:guid}")]
        public async Task<IActionResult> EditTrainingPointAsync(Guid id, CreateOrEditPointRequest request)
        {
            var point = await _trainingPointsRepository.GetByIdAsync(id);
            if (point == null)
                return BadRequest("Training point not found");

            point.TrainingId = request.TrainingId;
            point.Latitude = request.Latitude;
            point.Longitude = request.Longitude;
            point.DatePoint = request.DatePoint;

            await _trainingPointsRepository.AddOrUpdateAsync(point);

            var trainingPointAsyncName = "";//nameof(GetTrainingPointAsync);
            return CreatedAtAction(trainingPointAsyncName, new { id = point.Id }, null);
        }

        /// <summary>
        /// Delete training point
        /// </summary>
        /// <param name="id">Training point id</param>
        /// <returns></returns>
        [HttpDelete("{id:guid}")]
        public async Task<IActionResult> DeleteTrainingPointAsync(Guid id)
        {
            var point = await _trainingPointsRepository.GetByIdAsync(id);
            if (point == null)
                return NotFound();

            _cacheService.Remove(id.ToString());
            await _trainingPointsRepository.RemoveAsync(id);

            return NoContent();
        }
    }
}
