﻿using StackExchange.Redis;

namespace Explorers.DriveIn.TrainingPointsService.WebHost.Exstensions
{

    public static class ConfigureCacheExstension
    {
        public static IServiceCollection ConfigureRedisCache(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDistributedMemoryCache();

            var configurationOptions = ConfigurationOptions.Parse(configuration["Redis:Host"]);
            configurationOptions.User = configuration["Redis:User"];
            configurationOptions.Password = configuration["Redis:Password"];
            configurationOptions.ChannelPrefix = "RedisDemo:";

            services.AddStackExchangeRedisCache(options =>
            {
                options.ConfigurationOptions = configurationOptions;
            });
            return services;
        }
    }
}
