using Serilog;
using Explorers.DriveIn.TrainingPointsService.WebHost.Exstensions;
using Explorers.DriveIn.TrainingPointsService.Core.Abstractions.Repositories;
using Explorers.DriveIn.TrainingPointsService.DataAccess.Repositories;
using Microsoft.EntityFrameworkCore;
using Explorers.DriveIn.TrainingPointsService.DataAccess;
using Explorers.DriveIn.TrainingPointsService.DataAccess.Data;
using Explorers.DriveIn.TrainingPointsService.WebHost.Extensions;
using Explorers.DriveIn.TrainingPointsService.Core.Abstractions;
using Explorers.DriveIn.TrainingPointsService.Services;

var builder = WebApplication.CreateBuilder(args);

//Add logging
builder.Configuration.ConfigureLogging();
builder.Host.UseSerilog();

//Add Redis cache
builder.Services.ConfigureRedisCache(builder.Configuration);

// Add services to the container.
builder.Services.AddControllers(
    configure =>
    {
        configure.ReturnHttpNotAcceptable = true;
    });

builder.Services.AddMvc(options =>
{
    options.SuppressAsyncSuffixInActionNames = false;
});

// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

//Add models to container
builder.Services.AddScoped(typeof(IRepository<>), typeof(EfRepository<>));

//Add Database context
builder.Services.AddDbContext<DataContext>(options =>
{
    var connection = builder.Configuration.GetConnectionString("DriveInDatabase");
    options
        .UseNpgsql(builder.Configuration.GetConnectionString("DriveInDatabase"))
        .UseSnakeCaseNamingConvention();
});
builder.Services.AddScoped<IDbInitializer, EfDbInitializer>();

//Add cache service
builder.Services.AddScoped<ICacheService, CacheService>();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.ConfigureExceptionHandler(Log.Logger);

app.UseCors(opts =>
{
    var allowedCorsOrigins = builder.Configuration
        .GetSection("AllowedCorsOrigins")
        .AsEnumerable()
        .Select(x => x.Value)
        .Where(x => !string.IsNullOrEmpty(x))
        .ToArray();

    opts.WithOrigins(allowedCorsOrigins)
        .AllowCredentials()
        .AllowAnyMethod()
        .AllowAnyHeader();
});

app.UseAuthorization();

app.MapControllers();

//builder.Services.BuildServiceProvider().GetService<IDbInitializer>().InitializeDb();

app.Run();
