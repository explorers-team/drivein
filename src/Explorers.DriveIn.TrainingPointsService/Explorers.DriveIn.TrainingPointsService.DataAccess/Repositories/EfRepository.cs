﻿using Explorers.DriveIn.TrainingPointsService.Core.Abstractions.Repositories;
using Explorers.DriveIn.TrainingPointsService.Core.Domain;
using Microsoft.EntityFrameworkCore;

namespace Explorers.DriveIn.TrainingPointsService.DataAccess.Repositories
{
    public class EfRepository<T>
            : IRepository<T>
            where T : BaseEntity
    {
        protected readonly DataContext _dataContext;

        public EfRepository(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        /// <inheritdoc />
        public async Task AddAsync(T entity)
        {
            await _dataContext.Set<T>().AddAsync(entity);
            await _dataContext.SaveChangesAsync();
        }

        /// <inheritdoc />
        public async Task<bool> ExistsAsync(Guid id)
        {
            var entity = await _dataContext.Set<T>().FirstOrDefaultAsync(x => x.Id == id);
            return entity != null;
        }

        /// <inheritdoc />
        public async Task<IEnumerable<T>> GetAllAsync()
        {
            var entities = await _dataContext.Set<T>().ToListAsync();
            return entities;
        }

        /// <inheritdoc />
        public async Task<T> GetByIdAsync(Guid id)
        {
            var entity = await _dataContext.Set<T>().FirstOrDefaultAsync(x => x.Id == id);
            return entity;
        }

        /// <inheritdoc />
        public async Task<bool> RemoveAsync(Guid id)
        {
            var entity = await _dataContext.Set<T>().FirstOrDefaultAsync(x => x.Id == id);
            if (entity != null)
            {
                _dataContext.Set<T>().Remove(entity);
                return (await _dataContext.SaveChangesAsync()) > 0;
            }
            return false;
        }

        /// <inheritdoc />
        public async Task AddOrUpdateAsync(T entity)
        {
            _dataContext.Set<T>().Update(entity);
            await _dataContext.SaveChangesAsync();
        }
    }
}
