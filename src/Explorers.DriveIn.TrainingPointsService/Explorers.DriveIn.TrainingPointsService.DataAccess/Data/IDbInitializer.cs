﻿namespace Explorers.DriveIn.TrainingPointsService.DataAccess.Data
{
    public interface IDbInitializer
    {
        public void InitializeDb();
    }
}
