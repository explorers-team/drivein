﻿using Explorers.DriveIn.TrainingPointsService.Core.Domain;
using Microsoft.EntityFrameworkCore;

namespace Explorers.DriveIn.TrainingPointsService.DataAccess
{
    public class DataContext : DbContext
    {
        public DbSet<TrainingPoint> TrainingPoints { get; set; }

        /// <summary>
        /// DataContext constructor
        /// </summary>
        /// <param name="options"></param>
        public DataContext(DbContextOptions<DataContext> options)
            : base(options)
        {

        }
    }
}
