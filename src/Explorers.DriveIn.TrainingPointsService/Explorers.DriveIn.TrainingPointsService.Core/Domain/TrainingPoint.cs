﻿namespace Explorers.DriveIn.TrainingPointsService.Core.Domain
{
    /// <summary>
    /// Training points
    /// </summary>
    public class TrainingPoint : BaseEntity
    {
        /// <summary>
        /// Point latitude
        /// </summary>
        public double Latitude { get; set; }

        /// <summary>
        /// Ping longitude
        /// </summary>
        public double Longitude { get; set; }

        /// <summary>
        /// Date point
        /// </summary>
        public DateTime DatePoint { get; set; }

        /// <summary>
        /// Training id.
        /// </summary>
        public Guid TrainingId { get; set; }
    }
}
