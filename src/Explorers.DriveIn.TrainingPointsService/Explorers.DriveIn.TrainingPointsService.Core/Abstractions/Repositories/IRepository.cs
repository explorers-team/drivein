﻿using Explorers.DriveIn.TrainingPointsService.Core.Domain;

namespace Explorers.DriveIn.TrainingPointsService.Core.Abstractions.Repositories
{
    public interface IRepository<T>
            where T : BaseEntity
    {
        /// <summary>
        /// Getting all entities
        /// </summary>
        /// <returns>Entity collection</returns>
        Task<IEnumerable<T>> GetAllAsync();

        /// <summary>
        /// Get entity by id
        /// </summary>
        /// <param name="id">Entity id</param>
        /// <returns>Entity</returns>
        Task<T> GetByIdAsync(Guid id);

        /// <summary>
        /// Add entity
        /// </summary>
        /// <param name="item">Entity</param>
        Task AddAsync(T item);

        /// <summary>
        /// Remove entity by id
        /// </summary>
        /// <param name="id">Entity id</param>
        /// <returns>Removing result </returns>
        Task<bool> RemoveAsync(Guid id);

        /// <summary>
        /// Return exists of item by id
        /// </summary>
        /// <param name="id">Entity id</param>
        /// <returns>Item exists</returns>
        Task<bool> ExistsAsync(Guid id);

        /// <summary>
        /// Add or Update item
        /// </summary>
        /// <param name="id">Entity</param>
        Task AddOrUpdateAsync(T item);
    }
}
