using Xunit;

namespace Explorers.DriveIn.AuthService.UnitTests
{
    public static class PasswordHashHelperTest
    {
        /// <summary>
        /// Check password generate and validate operations works good.
        /// </summary>
        /// <returns></returns>
        [Theory]
        [InlineData("asdfg#1234Qwer")]
        public static void PasswordHashHelperShouldWorkCorrectly(string password)
        {
            var salt = PasswordHashHelper.GenerateSalt();
            var hashedSaltedPass = PasswordHashHelper.CreateHash(password, salt);
            Assert.True(PasswordHashHelper.Verify(password, salt, hashedSaltedPass));
        }
    }
}