﻿using Explorers.DriveIn.Abstractions.AuthService;
using Explorers.DriveIn.Core.Domain;
using Moq;
using Moq.AutoMock;
using Shouldly;
using Xunit;

namespace Explorers.DriveIn.AuthService.UnitTests;

public class AuthServiceUnitTests
{
    /// <summary>
    /// <see cref="AuthService.Login"/> should return valid token info if user exist and password is valid.
    /// </summary>
    [Fact]
    async Task AuthService_Login_Should_Return_ValidTokenInfo_IfUser_IsValid()
    {
        var autoMocker = new AutoMocker();

        var userRepository = autoMocker.GetMock<IUsersRepository>();
        userRepository.Setup(userRep => userRep.GetUserByPhoneNumber(It.IsAny<string>()))
            .Returns(Task.FromResult(new User()
            {
                Id = new Guid("8c09ca44-3158-443d-8303-ed87ab0d0507"),
                Salt = "LAuuiuTWKvX534S6G206Cw==",
                PasswordHash = "ZKR05jrAagwtH9tDPIrXeWHFmQeTMaZfmOr5paFzNL4=",
                PhoneNumber = "79991234567"
            }));
        
        var authService = autoMocker.CreateInstance<AuthService>();

        var loginResult = await authService.Login("79003050101", "asdfg#1234Qwer");
        loginResult.AccessToken.ShouldNotBeNullOrWhiteSpace();
        loginResult.RefreshToken.ShouldNotBeNullOrWhiteSpace();
    }


    /// <summary>
    /// <see cref="AuthService.CreatePasswordHash(string)"/> should return valid hash on password.
    /// </summary>
    [Fact]
    void AuthService_CreatePasswordHash_Should_Return_Valid_Hash()
    {
        //Arrange
        string password = "Qwerty123";
        var autoMocker = new AutoMocker();
        var authService = autoMocker.CreateInstance<AuthService>();
        var salt = PasswordHashHelper.GenerateSalt();
        var expectedHash = PasswordHashHelper.CreateHash(password, salt);

        //Act
        var passwordHash = authService.CreatePasswordHash(password, salt);

        //Assert
        Assert.Equal(expectedHash, passwordHash);
    }
}