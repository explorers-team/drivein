﻿using System.IdentityModel.Tokens.Jwt;
using System.Security.Authentication;
using System.Security.Claims;
using System.Text;
using Explorers.DriveIn.Abstractions.AuthService;
using Explorers.DriveIn.Core.Domain;
using Explorers.DriveIn.Core.Domain.Auth;
using Explorers.DriveIn.Core.Domain.Settings;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;

namespace Explorers.DriveIn.AuthService
{
    /// <summary>
    /// Authentication service. 
    /// </summary>
    public class AuthService : IAuthService
    {
        private readonly IUsersRepository _userRepository;
        private readonly IRefreshTokenRepository _refreshTokenRepository;
        private readonly IOptions<AuthSettings> _authSettings;

        /// <summary>
        /// JWT-token lifetime. 
        /// </summary>
        private readonly TimeSpan JWT_TOKEN_LIFETIME = TimeSpan.FromMinutes(60); 

        /// <summary>
        /// Refresh-token lifetime.
        /// </summary>
        private readonly TimeSpan REFRESH_TOKEN_LIFETIME = TimeSpan.FromMinutes(30); 

        public AuthService(
            IUsersRepository userRepository,
            IRefreshTokenRepository refreshTokenRepository,
            IOptions<AuthSettings> authSettings)
        {
            _userRepository = userRepository;
            _refreshTokenRepository = refreshTokenRepository;
            _authSettings = authSettings;
        }

        /// <summary>
        /// Log in user.
        /// Generate {JWT-token, Refresh-token} info.
        /// </summary>
        /// <param name="login">Login of user.</param>
        /// <param name="password">Password of user.</param>
        /// <returns>JWT- and Refresh-tokens information.</returns>
        /// <exception cref="AuthenticationException">If {<see cref="login"/>, <see cref="password"/>} is invalid.</exception>
        public async Task<TokenInfo> Login(string login, string password)
        {
            var user = await _userRepository.GetUserByPhoneNumber(login);

            if(user == null)
                throw new AuthenticationException("Register first");
                
            if(!ValidatePassword(password, user.Salt, user.PasswordHash))
                throw new AuthenticationException("Wrong credentials");

            var jwtToken = GenerateJwtToken(user.Id, user.PhoneNumber);

            var refreshToken = await CreateNewRefreshToken(user.Id);

            return new TokenInfo(user.FirstName, jwtToken, refreshToken.TokenValue);
        }

        /// <inheritdoc />
        public string CreatePasswordHash(string password, string salt)
        {
            return PasswordHashHelper.CreateHash(password, salt);
        }

        /// <summary>
        /// Create password hash
        /// </summary>
        /// <param name="password">Password</param>
        /// <returns>Generated hash</returns>
        public string CreatePasswordHash(string password)
        {
            return PasswordHashHelper.CreateHash(password, PasswordHashHelper.GenerateSalt());
        }

        /// <inheritdoc />
        public string GenerateSalt()
        {
            return PasswordHashHelper.GenerateSalt();
        }

        /// <summary>
        /// Validate that password equals to <see cref="userPasswordHash"/>.
        /// </summary>
        /// <param name="password">Password to validate.</param>
        /// <param name="userSalt">Salt to calculate hash.</param>
        /// <param name="userPasswordHash">Password hash.</param>
        /// <returns></returns>
        public bool ValidatePassword(string password, string userSalt, string userPasswordHash)
        {
            return PasswordHashHelper.Verify(password, userSalt, userPasswordHash);
        }

        /// <summary>
        /// Generate new pair {JWT-token, Refresh-token} information.
        /// </summary>
        /// <param name="refreshTokenValue">Value of current active refresh token.</param>
        /// <returns>Information about new tokens.</returns>
        /// <exception cref="AuthenticationException">Refresh-token is expired.</exception>
        public async Task<TokenInfo> RefreshJwtToken(string refreshTokenValue)
        {
            var currentRefreshToken = _refreshTokenRepository.GetByValue(refreshTokenValue);
            
            if(currentRefreshToken == null)
                throw new AuthenticationException("Refresh token not exist");

            if (currentRefreshToken.ExpiredAt <= DateTimeOffset.Now)
                throw new AuthenticationException("Refresh token is expired");

            var user = currentRefreshToken.User;
            var jwtToken = GenerateJwtToken(user.Id, user.PhoneNumber);
            var newRefreshToken = await CreateNewRefreshToken(user.Id);
            await _refreshTokenRepository.RemoveAsync(currentRefreshToken.Id);

            return new TokenInfo(user.FirstName, jwtToken, newRefreshToken.TokenValue);
        }

        private async Task<RefreshToken> CreateNewRefreshToken(Guid userId)
        {
            var newRefreshToken = new RefreshToken
            {
                UserId = userId,
                ExpiredAt = DateTimeOffset.Now.Add(REFRESH_TOKEN_LIFETIME).ToUniversalTime(),
                TokenValue = Guid.NewGuid().ToString()
            };
            await _refreshTokenRepository.AddAsync(newRefreshToken);

            return newRefreshToken;
        }

        private string GenerateJwtToken(Guid userId, string phoneNumber)
        {
            if (string.IsNullOrWhiteSpace(phoneNumber))
                throw new ArgumentNullException(nameof(phoneNumber));

            var jwt = new JwtSecurityToken(
                claims: new[]
                {
                    new Claim("userId", userId.ToString()),
                    new Claim("phoneNumber", phoneNumber),
                },
                expires: DateTime.UtcNow.Add(JWT_TOKEN_LIFETIME),
                signingCredentials: new SigningCredentials(GetSymmetricSecurityKey(), SecurityAlgorithms.HmacSha256));

            return new JwtSecurityTokenHandler().WriteToken(jwt);
        }

        private SymmetricSecurityKey GetSymmetricSecurityKey() =>
            new(Encoding.UTF8.GetBytes(_authSettings.Value.JwtSecret));
    }
}