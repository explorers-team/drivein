using System.Security.Cryptography;
using Microsoft.AspNetCore.Cryptography.KeyDerivation;

namespace Explorers.DriveIn.AuthService
{
    /// <summary>
    /// Password hash worker (generator/validator).
    /// <remarks>
    /// Source: https://docs.microsoft.com/en-us/aspnet/core/security/data-protection/consumer-apis/password-hashing?view=aspnetcore-6.0
    /// </remarks>
    /// </summary>
    public static class PasswordHashHelper
    {
        /// <summary>
        /// Вычислить хэш пароля. 
        /// </summary>
        /// <param name="password">Пароль.</param>
        /// <param name="salt">Соль.</param>
        /// <returns></returns>
        public static string CreateHash(string password, string salt)
        {
            // derive a 256-bit subkey (use HMACSHA512 with 100,000 iterations)
            var hashed = Convert.ToBase64String(
                KeyDerivation.Pbkdf2(
                    password: password,
                    salt: Convert.FromBase64String(salt),
                    prf: KeyDerivationPrf.HMACSHA512,
                    iterationCount: 100000,
                    numBytesRequested: 256 / 8));

            return hashed;
        }

        /// <summary>
        /// Проверить, что ожидаемый хэш <paramref name="expectedHash"/>
        /// совпадает c хэшем, вычисленным на основе <paramref name="password"/> и
        /// <paramref name="salt"/>.
        /// </summary>
        public static bool Verify(string password, string salt, string expectedHash)
        {
            var actualHash = CreateHash(password, salt);
            return string.Equals(actualHash, expectedHash);
        }

        /// <summary>
        /// Cгенерировать 128-битную соль, используя криптографически стойкую случайную последовательность ненулевых значений.
        /// </summary>
        /// <returns></returns>
        public static string GenerateSalt()
        {
            var salt = new byte[128 / 8];
            using var rngCsp = new RNGCryptoServiceProvider();
            rngCsp.GetNonZeroBytes(salt);
            return Convert.ToBase64String(salt);
        }
    }
}