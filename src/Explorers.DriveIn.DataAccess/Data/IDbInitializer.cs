﻿namespace Explorers.DriveIn.DataAccess.Data
{
    public interface IDbInitializer
    {
        public void InitializeDb();
    }
}
