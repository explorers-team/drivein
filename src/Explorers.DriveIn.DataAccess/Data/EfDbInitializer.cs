﻿using Explorers.DriveIn.Core.Domain;
using Microsoft.EntityFrameworkCore;

namespace Explorers.DriveIn.DataAccess.Data
{
    public class EfDbInitializer
        : IDbInitializer
    {
        private readonly DataContext _dataContext;

        public EfDbInitializer(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public void InitializeDb()
        {
            _dataContext.Database.Migrate();
            AddFirstUsers(_dataContext);
        }

        private void AddFirstUsers(DataContext dataContext)
        {
            var firstUser = dataContext.Users.FirstOrDefault(u => u.PhoneNumber == "70000000000");
            var firstUser2 = dataContext.Users.FirstOrDefault(u => u.PhoneNumber == "70000000001");

            if (firstUser == null)
            {
                dataContext.Users.Add(new User
                {
                    Id = new Guid("A8DEB538-44E5-4962-8783-15A414D23F02"),
                    FirstName = "Admin",
                    SecondName = "Adminov",
                    PhoneNumber = "70000000000",
                    Email = "admin@drivein.com",
                    Salt = "OBx4ohIjLQQ457dWYNWy9g==",
                    PasswordHash = "AtHc2XLGOJ1nvzj2Zv+UzEKClbnckERVLTfAlv3mTjw=" // пароль "Qwerty123"
                });
            }
            
            if (firstUser2 == null)
            {
                dataContext.Users.Add(new User
                {
                    Id = new Guid("980C1E29-2950-4E27-BE50-8E41A4D6A19D"),
                    FirstName = "Ivan",
                    SecondName = "Ivanov",
                    PhoneNumber = "70000000001",
                    Email = "admin@drivein.com",
                    Salt = "OBx4ohIjLQQ457dWYNWy9g==",
                    PasswordHash = "AtHc2XLGOJ1nvzj2Zv+UzEKClbnckERVLTfAlv3mTjw=" // пароль "Qwerty123"
                });
            }

            dataContext.SaveChanges();
        }
    }

}
