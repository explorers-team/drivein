﻿using Explorers.DriveIn.Core.Domain;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Explorers.DriveIn.Core.Domain.Auth;

namespace Explorers.DriveIn.DataAccess
{
    public class DataContext : DbContext
    {
        public DbSet<User?> Users { get; set; }

        public DbSet<UserFollower> UserFollowers { get; set; }

        public DbSet<Training> Trainings { get; set; }

        public DbSet<TrainingPoint> TrainingPoints { get; set; }

        public DbSet<TrainingLike> TrainingLikes { get; set; }
        
        public DbSet<RefreshToken> RefreshTokens { get; set; }

        /// <summary>
        /// DataContext constructor
        /// </summary>
        /// <param name="options"></param>
        public DataContext(DbContextOptions<DataContext> options)
            : base(options)
        {

        }
    }
}
