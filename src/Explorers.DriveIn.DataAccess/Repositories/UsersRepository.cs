﻿using Explorers.DriveIn.Abstractions.AuthService;
using Explorers.DriveIn.Core.Domain;
using Microsoft.EntityFrameworkCore;

namespace Explorers.DriveIn.DataAccess.Repositories;

/// <summary>
/// Repository to act with user.
/// </summary>
public class UsersRepository : EfRepository<User>, IUsersRepository
{
    public UsersRepository(DataContext dataContext) : base(dataContext)
    {
    }

    public async Task<User?> GetUserByPhoneNumber(string phoneNumber)
    {
        return await _dataContext.Users.FirstOrDefaultAsync(user => user.PhoneNumber == phoneNumber);
    }

    public User GetUserByRefreshToken(string refreshToken)
    {
        throw new NotImplementedException();
    }
}