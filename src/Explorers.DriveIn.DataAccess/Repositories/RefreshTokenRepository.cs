﻿using Explorers.DriveIn.Abstractions.AuthService;
using Explorers.DriveIn.Core.Domain.Auth;
using Microsoft.EntityFrameworkCore;

namespace Explorers.DriveIn.DataAccess.Repositories;

public class RefreshTokenRepository : EfRepository<RefreshToken>, IRefreshTokenRepository
{
    public RefreshTokenRepository(DataContext dataContext) : base(dataContext)
    {
    }

    public RefreshToken GetByValue(string refreshToken)
    {
        return _dataContext
            .RefreshTokens
            .Include(rt => rt.User)
            .FirstOrDefault(rt => rt.TokenValue == refreshToken);
    }
}