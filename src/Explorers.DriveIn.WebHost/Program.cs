using System.Configuration;
using Explorers.DriveIn.Abstractions;
using Explorers.DriveIn.DataAccess;
using Microsoft.EntityFrameworkCore;
using Explorers.DriveIn.DataAccess.Repositories;
using Explorers.DriveIn.Core.Domain;
using Explorers.DriveIn.DataAccess.Data;
using Serilog;
using Explorers.DriveIn.AuthService;
using Explorers.DriveIn.Abstractions.AuthService;
using Serilog.Sinks.Elasticsearch;
using System.Reflection;
using Explorers.DriveIn.WebHost.Extensions;
using Explorers.DriveIn.Core.Domain.Settings;
using Explorers.DriveIn.WebHost.Controllers.Auth;
using FluentValidation.AspNetCore;
using FluentValidation;
using Explorers.DriveIn.WebHost.ValidationRules;
using Explorers.DriveIn.Abstractions.Gateways;
using Explorers.DriveIn.Abstractions.MessageQueue;
using Explorers.DriveIn.Integration;
using Explorers.DriveIn.WebHost.Chat;

var builder = WebApplication.CreateBuilder(args);

//Logger
ConfigureLogging();
builder.Host.UseSerilog();


// Add services to the container.
builder.Services.AddControllers(
    configure =>
    {
        configure.ReturnHttpNotAcceptable = true;
    });

builder.Services.AddMvc(options =>
{
    options.SuppressAsyncSuffixInActionNames = false;
});

// Add SignalR support
builder.Services.AddSignalR();

// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

//Add models to container
builder.Services.AddScoped(typeof(IRepository<>), typeof(EfRepository<>));
builder.Services.AddScoped<IUsersRepository, UsersRepository>();
builder.Services.AddScoped<IRefreshTokenRepository, RefreshTokenRepository>();

//Add Authentication service
builder.Services.AddScoped(typeof(IAuthService), typeof(AuthService));

//Add validation
builder.Services.AddValidatorsFromAssemblyContaining<CreateOrEditUserRequestValudator>();

//Add integration 
builder.Services.AddHttpClient<ITrainingPointsGateway, TrainingPointsGateway>(c =>
{
    c.BaseAddress = new Uri(builder.Configuration["IntegrationSettings:TrainingPointsApiUrl"]);
});

//Add Message broker
builder.Services.AddScoped<INotificationMqService, NotificationMqService>();

//Add Database context
builder.Services.AddDbContext<DataContext>(options =>
{
    //options.UseSqlite("Filename=DriveInDatabase.sqlite", b => b.MigrationsAssembly("Explorers.DriveIn.DataAccess"));
    var connection = builder.Configuration.GetConnectionString("DriveInDatabase");
    Console.WriteLine($"DB connection string: {connection}"); // исключительно для отладки, в проде такого, конечно, не будет.
    options
        .UseNpgsql(builder.Configuration.GetConnectionString("DriveInDatabase"))
        .UseSnakeCaseNamingConvention();
});

builder.Services.AddScoped<IDbInitializer, EfDbInitializer>();

var jwtSettings = builder.Configuration.GetSection("AuthSettings");
builder.Services.Configure<AuthSettings>(jwtSettings);

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.ConfigureExceptionHandler(Log.Logger);

// app.UseHttpsRedirection();

app.UseCors(opts =>
{
    var allowedCorsOrigins = builder.Configuration
        .GetSection("AllowedCorsOrigins")
        .AsEnumerable()
        .Select(x => x.Value)
        .Where(x => !string.IsNullOrEmpty(x))
        .ToArray();

    opts.WithOrigins(allowedCorsOrigins)
        .AllowCredentials()
        .AllowAnyMethod()
        .AllowAnyHeader();
});

app.UseRouting();
app.UseAuthorization();

app.UseEndpoints(endpoints =>
{
    endpoints.MapControllers();
    // Add SignalR support part.2
    endpoints.MapHub<ChatHub>("/hubs/chat");
});

builder.Services.BuildServiceProvider().GetService<IDbInitializer>().InitializeDb();

app.Run();


void ConfigureLogging()
{
    var environment = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
    var configuration = new ConfigurationBuilder()
        .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
        .AddJsonFile(
            $"appsettings.{Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT")}.json",
            optional: true)
        .Build();
    
    Log.Logger = new LoggerConfiguration()
        .Enrich.FromLogContext()
        .WriteTo.Debug()
        .WriteTo.Console()
        .WriteTo.Elasticsearch(ConfigureElasticSink(configuration, environment))
        .Enrich.WithProperty("Environment", environment)
        .ReadFrom.Configuration(configuration)
        .CreateLogger();
}

ElasticsearchSinkOptions ConfigureElasticSink(IConfigurationRoot configuration, string environment)
{
    if (String.IsNullOrWhiteSpace(configuration["ElasticConfiguration:Uri"]))
        return new ElasticsearchSinkOptions();
    
    return new ElasticsearchSinkOptions(new Uri(configuration["ElasticConfiguration:Uri"]))
    {
        AutoRegisterTemplate = true,
        IndexFormat = $"{Assembly.GetExecutingAssembly().GetName().Name.ToLower().Replace(".", "-")}-{environment?.ToLower().Replace(".", "-")}-{DateTime.UtcNow:yyyy-MM}"
    };
}

