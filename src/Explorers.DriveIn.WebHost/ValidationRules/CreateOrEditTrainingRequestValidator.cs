﻿using Explorers.DriveIn.WebHost.Models;
using FluentValidation;

namespace Explorers.DriveIn.WebHost.ValidationRules
{
    public class CreateOrEditTrainingRequestValidator :AbstractValidator<CreateOrEditTrainingRequest>
    {
        public CreateOrEditTrainingRequestValidator()
        {
            RuleFor(tr => tr.DateBegin).NotNull().WithMessage("DateBegin must be not null");
            RuleFor(tr => tr.DateEnd).NotNull().WithMessage("DateEnd must be not null");
            RuleFor(tr => tr.Distance).GreaterThan(0).WithMessage("Invalid distance");
        }
    }
}
