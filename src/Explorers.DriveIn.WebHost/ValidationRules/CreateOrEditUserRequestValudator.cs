﻿using Explorers.DriveIn.WebHost.Models;
using FluentValidation;

namespace Explorers.DriveIn.WebHost.ValidationRules
{
    public class CreateOrEditUserRequestValudator : AbstractValidator<CreateOrEditUserRequest>
    {
        public CreateOrEditUserRequestValudator()
        {

            RuleFor(u => u.FirstName).NotNull().NotEmpty().Length(2, 30).WithMessage("First name must be between 2 and 30 character");
            RuleFor(u => u.SecondName).NotNull().NotEmpty().Length(2, 30).WithMessage("First name must be between 2 and 30 character"); ;
            RuleFor(u => u.Email).EmailAddress().WithMessage("Invalid Email address");
            RuleFor(u => u.PhoneNumber).
                Matches(@"^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$").WithMessage("Phone number must be not empty");

            RuleFor(u => u.Password).NotEmpty().WithMessage("Password cannot be empty")
                .MinimumLength(8).WithMessage("Password length must be at least 8.")
                .MaximumLength(16).WithMessage("Password length must not exceed 16.")
                .Matches(@"[A-Z]+").WithMessage("Password must contain at least one uppercase letter.")
                .Matches(@"[a-z]+").WithMessage("Password must contain at least one lowercase letter.")
                .Matches(@"[0-9]+").WithMessage("Password must contain at least one number.")
                .Matches(@"[\!\?\*\.]+").WithMessage("Password must contain at least one (!? *.).");
        }
    }
}
