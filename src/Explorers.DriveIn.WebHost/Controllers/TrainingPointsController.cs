﻿using Explorers.DriveIn.Abstractions;
using Explorers.DriveIn.Abstractions.Gateways;
using Explorers.DriveIn.Core.Domain;
using Explorers.DriveIn.WebHost.Controllers.Auth;
using Explorers.DriveIn.WebHost.Models;
using Microsoft.AspNetCore.Mvc;

namespace Explorers.DriveIn.WebHost.Controllers
{
    /// <summary>
    /// Training points
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    [DriveInAuthorize]
    public class TrainingPointsController : Controller
    {
        //Models context
        private readonly IRepository<Training> _trainingsRepository;
        private readonly ITrainingPointsGateway _trainingPointsGateway;

        /// <summary>
        /// Training points controller constructor
        /// </summary>
        /// <param name="trainingsRepository"></param>
        /// <param name="trainingPointsGateway"></param>
        public TrainingPointsController(IRepository<Training> trainingsRepository, ITrainingPointsGateway trainingPointsGateway)
        {
            _trainingsRepository = trainingsRepository;
            _trainingPointsGateway = trainingPointsGateway;
        }

        /// <summary>
        /// Get all training points
        /// </summary>
        /// <returns>Training points collection</returns>
        [HttpGet]
        public async Task<ActionResult<List<TrainingPoint>>> GetTrainingPointsAsync()
        {
            var points = await _trainingPointsGateway.GetTrainingPointsAsync();
            return Ok(points);
        }

        /// <summary>
        /// Get training point by Id  
        /// </summary>
        /// <returns>Training point</returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<TrainingPoint>> GetTrainingPointAsync(Guid id)
        {
            var point = await _trainingPointsGateway.GetTrainingPointsByIdAsync(id);
            if (point == null)
                return NotFound();
            else
                return Ok(point);
        }

        /// <summary>
        /// Get training point by training Id  
        /// </summary>
        /// <param name="trainingId">Training id</param>
        /// <returns>Training points</returns>
        [Route("by-training-id/{trainingId:guid}")]
        [HttpGet]
        public async Task<ActionResult<List<TrainingPoint>>> GetTrainingPointsByTrainingId(Guid trainingId)
        {
            var trainingPoints = await _trainingPointsGateway.GetTrainingPointsByTrainingIdAsync(trainingId);

            if (trainingPoints == null)
                return NotFound();
            else
                return Ok(trainingPoints);
        }

        /// <summary>
        /// Create training point
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> CreateTrainingPointAsync(CreateOrEditTrainingPointRequest request)
        {
            var userId = HttpContext.GetUserId();
            
            var training = await _trainingsRepository.GetByIdAsync(request.TrainingId);
            if (training == null)
                return BadRequest("Training not found");

            if (training.UserId != userId)
                return Forbid(); // 403 

            TrainingPoint point = new TrainingPoint
            {
                Training = training,
                Latitude = request.Latitude,
                Longitude = request.Longitude,
                DatePoint = request.DatePoint
            };

            await _trainingPointsGateway.AddOrUpdateTrainingPointAsync(point);

            var trainingPointAsyncName = nameof(GetTrainingPointAsync);
            return CreatedAtAction(trainingPointAsyncName, new { id = point.Id }, null);
        }

        /// <summary>
        /// Update training point
        /// </summary>
        /// <param name="id">Training point id</param>
        /// <param name="request">Update request</param>
        /// <returns></returns>
        [HttpPut("{id:guid}")]
        public async Task<IActionResult> EditTrainingPointAsync(Guid id, CreateOrEditTrainingPointRequest request)
        {
            var training = await _trainingsRepository.GetByIdAsync(request.TrainingId);
            if (training == null)
                return BadRequest("Training not found");

            var point = await _trainingPointsGateway.GetTrainingPointsByIdAsync(id);
            if (point == null)
                return BadRequest("Training point not found");

            point.Training = training;
            point.Latitude = request.Latitude;
            point.Longitude = request.Longitude;
            point.DatePoint = request.DatePoint;

            await _trainingPointsGateway.AddOrUpdateTrainingPointAsync(point);

            var trainingPointAsyncName = nameof(GetTrainingPointAsync);
            return CreatedAtAction(trainingPointAsyncName, new { id = point.Id }, null);
        }

        /// <summary>
        /// Delete training point
        /// </summary>
        /// <param name="id">Training point id</param>
        /// <returns></returns>
        [HttpDelete("{id:guid}")]
        public async Task<IActionResult> DeleteTrainingPointAsync(Guid id)
        {
            var point = await _trainingPointsGateway.GetTrainingPointsByIdAsync(id);

            await _trainingPointsGateway.DeleteTrainingPointAsync(id);

            return NoContent();
        }
    }
}
