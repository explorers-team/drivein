﻿using Explorers.DriveIn.Abstractions;
using Explorers.DriveIn.Core.Domain;
using Explorers.DriveIn.WebHost.Controllers.Auth;
using Explorers.DriveIn.WebHost.Models;
using Microsoft.AspNetCore.Mvc;

namespace Explorers.DriveIn.WebHost.Controllers
{
    /// <summary>
    /// Training likes
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    [DriveInAuthorize]
    public class TrainingLikesController : Controller
    {
        //Models context
        private readonly IRepository<Training> _trainingsRepository;
        private readonly IRepository<TrainingLike> _trainingLikesRepository;
        private readonly IRepository<User> _usersRepository;

        /// <summary>
        /// Training likes controller constructor
        /// </summary>
        /// <param name="trainingsRepository"></param>
        /// <param name="trainingLikesRepository"></param>
        public TrainingLikesController(IRepository<Training> trainingsRepository, IRepository<TrainingLike> trainingLikesRepository,
            IRepository<User> usersRepository)
        {
            _trainingsRepository = trainingsRepository;
            _trainingLikesRepository = trainingLikesRepository;
            _usersRepository = usersRepository;
        }

        /// <summary>
        /// Get all training likes
        /// </summary>
        /// <returns>Training likes collection</returns>
        [HttpGet]
        public async Task<ActionResult<List<TrainingLike>>> GetTrainingLikesAsync()
        {
            var likes = await _trainingLikesRepository.GetAllAsync();
            return Ok(likes);
        }

        /// <summary>
        /// Get training like by Id  
        /// </summary>
        /// <returns>Training like</returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<TrainingLike>> GetTrainingLikeAsync(Guid id)
        {
            var like = await _trainingLikesRepository.GetByIdAsync(id);
            if (like == null)
                return NotFound();
            else
                return Ok(like);
        }

        /// <summary>
        /// Get training like by training Id  
        /// </summary>
        /// <param name="trainingId">Training id</param>
        /// <returns>Training likes</returns>
        [Route("by-training-id/{id:guid}")]
        [HttpGet]
        public async Task<ActionResult<List<TrainingLike>>> GetTrainingLikesByTrainingId(Guid trainingId)
        {
            var likes = await _trainingLikesRepository.GetAllAsync();

            var trainingLikes = likes.Where(l => l.Training.Id == trainingId);

            if (trainingLikes == null)
                return NotFound();
            else
                return Ok(trainingLikes);
        }

        /// <summary>
        /// Create training like
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> CreateTrainingLikeAsync(CreateOrEditTrainingLikeRequest request)
        {
            var training = await _trainingsRepository.GetByIdAsync(request.TrainingId);
            if (training == null)
                return BadRequest("Training not found");

            var user = await _usersRepository.GetByIdAsync(request.UserId);
            if (user == null)
                return BadRequest("User not found");

            TrainingLike like = new TrainingLike
            {
                Training = training,
                User = user,
                DateLike = request.DateLike
            };

            await _trainingLikesRepository.AddAsync(like);

            var trainingLikeAsyncName = nameof(GetTrainingLikeAsync);
            return CreatedAtAction(trainingLikeAsyncName, new { id = like.Id }, null);
        }

        /// <summary>
        /// Update training like
        /// </summary>
        /// <param name="id">Training like id</param>
        /// <param name="request">Update request</param>
        /// <returns></returns>
        [HttpPut("{id:guid}")]
        public async Task<IActionResult> EditTrainingLikeAsync(Guid id, CreateOrEditTrainingLikeRequest request)
        {
            var training = await _trainingsRepository.GetByIdAsync(request.TrainingId);
            if (training == null)
                return BadRequest("Training not found");

            var user = await _usersRepository.GetByIdAsync(request.UserId);
            if (user == null)
                return BadRequest("User not found");

            var like = await _trainingLikesRepository.GetByIdAsync(id);
            if (like == null)
                return BadRequest("Training like not found");

            like.User = user;
            like.Training = training;
            like.DateLike = request.DateLike;

            await _trainingLikesRepository.AddOrUpdateAsync(like);

            var trainingLikeAsyncName = nameof(GetTrainingLikeAsync);
            return CreatedAtAction(trainingLikeAsyncName, new { id = like.Id }, null);
        }

        /// <summary>
        /// Delete training like
        /// </summary>
        /// <param name="id">Training like id</param>
        /// <returns></returns>
        [HttpDelete("{id:guid}")]
        public async Task<IActionResult> DeleteTrainingLikeAsync(Guid id)
        {
            var like = await _trainingLikesRepository.GetByIdAsync(id);
            if (like == null)
                return NotFound();

            await _trainingLikesRepository.RemoveAsync(id);

            return NoContent();
        }
    }
}
