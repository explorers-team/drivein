﻿using System.ComponentModel.DataAnnotations;

namespace Explorers.DriveIn.WebHost.Controllers.Auth;

/// <summary>
/// Information to login user.
/// </summary>
public class LoginRequestDto
{
    /// <summary>
    /// Login of user account.
    /// </summary>
    [Required]
    public string? Login { get; set; }

    /// <summary>
    /// Password of user account to verification.
    /// </summary>
    [Required]
    public string? Password { get; set; }
}