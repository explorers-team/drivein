﻿using Explorers.DriveIn.Abstractions.AuthService;
using Explorers.DriveIn.Core.Domain.Auth;
using Microsoft.AspNetCore.Mvc;

namespace Explorers.DriveIn.WebHost.Controllers.Auth;

/// <summary>
/// Authentication.
/// </summary>
[Route("api/v1/auth")]
[ApiController]
public class AuthController : Controller
{
    private readonly IAuthService _authService;

    public AuthController(IAuthService authService)
    {
        _authService = authService;
    }
    
    /// <summary>
    /// Method to sign in user.
    /// </summary>
    /// <responce code="200">Success.</responce>
    /// <responce code="403">Access denied.</responce> 
    /// <response code="400">Bad Request.</response>
    [HttpPost]
    [Route("login")]
    [ProducesResponseType(typeof(TokenInfo), StatusCodes.Status200OK)]
    [ProducesResponseType(typeof(object), StatusCodes.Status401Unauthorized)]
    public async Task<IActionResult> Login(LoginRequestDto loginRequestDto)
    {
        try
        {
            TokenInfo loginResult = await _authService.Login(loginRequestDto.Login, loginRequestDto.Password);
            return Ok(loginResult);
        }
        catch (UnauthorizedAccessException e) 
        {
            return Unauthorized(null);
        }
    }

    /// <summary>
    /// Обновить Refresh-токен. 
    /// </summary>
    /// <returns>Данные о новых Refresh- и Access-токенах.</returns>
    [HttpPost]
    [Route("access-token/refresh")]
    [ProducesResponseType(typeof(TokenInfo), StatusCodes.Status200OK)]
    [ProducesResponseType(typeof(object), StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(typeof(object), StatusCodes.Status400BadRequest)]
    public async Task<IActionResult> RefreshAccessToken(string refreshToken)
    {
        if (string.IsNullOrWhiteSpace(refreshToken))
            return BadRequest();
        
        TokenInfo tokenInfo = await _authService.RefreshJwtToken(refreshToken);
        return Ok(tokenInfo);
    }
}