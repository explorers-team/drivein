﻿namespace Explorers.DriveIn.WebHost.Controllers.Auth
{
    /// <summary>
    /// Extension methods for working with the HTTP context.
    /// </summary>
    public static class HttpContextExtension
    {
        /// <summary>
        /// Get user ID.
        /// </summary>
        /// <param name="httpContext">HTTP request context.</param>
        /// <exception cref="InvalidOperationException">If UserId is not in context.</exception>
        public static Guid GetUserId(this HttpContext httpContext)
        {
            var userId = httpContext.Items["UserId"]?.ToString() ??
                         throw new InvalidOperationException("UserId missing");
            if(!Guid.TryParse(userId, out var userIdGuid))
                throw new InvalidCastException("Bad user id format.");
            
            return userIdGuid;
        }

        /// <summary>
        /// Set the user ID to context.
        /// </summary>
        public static void SetUserId(this HttpContext httpContext, Guid userId)
        {
            httpContext.Items["UserId"] = userId;
        }
    }
}