﻿using System.IdentityModel.Tokens.Jwt;
using System.Text;
using Explorers.DriveIn.Core.Domain.Settings;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;

namespace Explorers.DriveIn.WebHost.Controllers.Auth;

/// <summary>
/// Validate the JWT token.
/// </summary>
public class DriveInAuthorizeAttribute : Attribute, IAuthorizationFilter
{
    private AuthSettings? _authSettings;

    public void OnAuthorization(AuthorizationFilterContext context)
    {
        var services = context.HttpContext.RequestServices;
        _authSettings = services.GetService<IOptions<AuthSettings>>()?.Value;

        var authHeader = context.HttpContext.Request.Headers["Authorization"]
            .FirstOrDefault();

        if (string.IsNullOrWhiteSpace(authHeader))
            throw new UnauthorizedAccessException("Authorization header missing");

        string? token = null;
        if (authHeader.StartsWith("Bearer ", StringComparison.OrdinalIgnoreCase))
        {
            token = authHeader.Substring("Bearer ".Length).Trim();
        }

        if (string.IsNullOrWhiteSpace(token))
            throw new UnauthorizedAccessException();

        Guid userId = ParseToken(token);

        context.HttpContext.SetUserId(userId);
    }

    /// <summary>
    /// Get the user ID from the token.
    /// </summary>
    private Guid ParseToken(string token)
    {
        try
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.UTF8.GetBytes(_authSettings.JwtSecret);

            tokenHandler.ValidateToken(token, new TokenValidationParameters
            {
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = new SymmetricSecurityKey(key),
                ValidateIssuer = false,
                ValidateAudience = false,
                // set clock shift to zero so tokens will expire exactly
                // when the token expires (not after 5 minutes)
                ClockSkew = TimeSpan.Zero
            }, out SecurityToken validatedToken);

            var jwtToken = (JwtSecurityToken)validatedToken;

            var userId = jwtToken.Claims.FirstOrDefault(claim => claim.Type == "userId")!.Value;

            return Guid.Parse(userId);
        }
        catch(SecurityTokenInvalidLifetimeException)
        {
            throw new UnauthorizedAccessException();
        }
        catch(SecurityTokenExpiredException)
        {
            throw new UnauthorizedAccessException();
        }
    }
}