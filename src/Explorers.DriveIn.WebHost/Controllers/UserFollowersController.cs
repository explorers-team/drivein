﻿using Explorers.DriveIn.Abstractions;
using Explorers.DriveIn.Abstractions.MessageQueue;
using Explorers.DriveIn.Core.Domain;
using Explorers.DriveIn.WebHost.Controllers.Auth;
using Microsoft.AspNetCore.Mvc;

namespace Explorers.DriveIn.WebHost.Controllers
{
    /// <summary>
    /// User followers
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    [DriveInAuthorize]
    public class UserFollowersController : Controller
    {
        //Models context
        private readonly IRepository<UserFollower> _userFoolowersRepository;
        private readonly INotificationMqService _notificationMqService;

        /// <summary>
        /// User followers constructor 
        /// </summary>
        /// <param name="userFoolowersRepository"></param>
        public UserFollowersController(IRepository<UserFollower> userFoolowersRepository, INotificationMqService notificationMqService)
        {
            _userFoolowersRepository = userFoolowersRepository;
            _notificationMqService = notificationMqService;
        }

        /// <summary>
        /// Get all followers
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<UserFollower>>> GetAllFollowersAsync()
        {
            var userFollowers = await _userFoolowersRepository.GetAllAsync();
            return Ok(userFollowers);
        }

        /// <summary>
        /// Get follower by id  
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<UserFollower>> GetFollowerAsync(Guid id)
        {
            var follower = await _userFoolowersRepository.GetByIdAsync(id);
            if (follower == null)
                return NotFound();
            else
                return Ok(follower);
        }

        /// <summary>
        /// Create user follower
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> CreateUserFollowerAsync(UserFollower request)
        {
            UserFollower userFollower = new UserFollower
            {
                User = request.User,
                Follower = request.Follower,
                DateFollow = DateTime.Now
            };

            await _userFoolowersRepository.AddAsync(userFollower);

            _notificationMqService.SendMessage(userFollower);

            var followerAsyncName = nameof(GetFollowerAsync);
            return CreatedAtAction(followerAsyncName, new { id = userFollower.Id }, null);
        }

        /// <summary>
        /// Delete user follower
        /// </summary>
        /// <param name="id">User follower id</param>
        /// <returns></returns>
        [HttpDelete("{id:guid}")]
        public async Task<IActionResult> DeleteUserAsync(Guid id)
        {
            var user = await _userFoolowersRepository.GetByIdAsync(id);
            if (user == null)
                return NotFound();

            await _userFoolowersRepository.RemoveAsync(id);

            return NoContent();
        }
    }
}
