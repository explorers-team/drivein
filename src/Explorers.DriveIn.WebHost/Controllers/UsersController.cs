using Explorers.DriveIn.Abstractions.AuthService;
using Explorers.DriveIn.Abstractions;
using Explorers.DriveIn.Core.Domain;
using Explorers.DriveIn.WebHost.Controllers.Auth;
using Explorers.DriveIn.WebHost.Models;
using Microsoft.AspNetCore.Mvc;
using FluentValidation;
using FluentValidation.Results;
using Explorers.DriveIn.WebHost.Extensions;

namespace Explorers.DriveIn.WebHost.Controllers
{
    /// <summary>
    /// Users
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    [DriveInAuthorize]
    public class UsersController : ControllerBase
    {
        //Models context
        private readonly IRepository<User> _userRepository;
        private readonly IAuthService _authService;
        private readonly ILogger<UsersController> _logger;
        private readonly IValidator<CreateOrEditUserRequest> _validator;

        /// <summary>
        /// Users controller constructor
        /// </summary>
        /// <param name="context"></param>
        public UsersController(IRepository<User> userRepository, 
            IAuthService authService, 
            ILogger<UsersController> logger,
            IValidator<CreateOrEditUserRequest> validator)
        {
            _userRepository = userRepository;
            _authService = authService;
            _logger = logger;
            _validator = validator;
        }

        /// <summary>
        /// Get all users
        /// </summary>
        /// <returns>Users collection</returns>
        [HttpGet]
        public async Task<ActionResult<List<User>>> GetUsersAsync()
        {
            var users = await _userRepository.GetAllAsync();
            return Ok(users);
        }

        /// <summary>
        /// Get user by Id  
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<User>> GetUserAsync(Guid id)
        {
            var user = await _userRepository.GetByIdAsync(id);
            if (user == null)
                return NotFound();
            else
                return Ok(user);
        }

        /// <summary>
        /// Create user
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> CreateUserAsync(CreateOrEditUserRequest request)
        {
            ValidationResult validationResult = await _validator.ValidateAsync(request);
            if(!validationResult.IsValid)
            {
                validationResult.AddToModelState(this.ModelState);
                return BadRequest(validationResult);
            }

            var users = await _userRepository.GetAllAsync();

            var user = users.Where(u => u.Email == request.Email);
            if (user != null && user.Count() > 0)
                return BadRequest("User already exists");

            string salt = _authService.GenerateSalt();
            string passwordHash = _authService.CreatePasswordHash(request.Password, salt);

            User newUser = new User
            {
                Id = Guid.NewGuid(),
                FirstName = request.FirstName,
                SecondName = request.SecondName,
                Email = request.Email,
                PhoneNumber = request.PhoneNumber,
                DateRegistration = DateTime.Now,
                PasswordHash = passwordHash,
                Salt = salt
            };

            await _userRepository.AddAsync(newUser);

            var userAsyncName = nameof(GetUserAsync);
            return CreatedAtAction(userAsyncName, new { id = newUser.Id }, null);
        }

        /// <summary>
        /// Edit user
        /// </summary>
        /// <returns></returns>
        [HttpPut("{id:guid}")]
        public async Task<IActionResult> EditUserAsync(Guid id, CreateOrEditUserRequest request)
        {
            var user = await _userRepository.GetByIdAsync(id);

            if (user == null || user.Id == Guid.Empty)
                return NotFound();

            user.FirstName = request.FirstName;
            user.SecondName = request.SecondName;
            user.PhoneNumber = request.PhoneNumber;
            user.Email = request.Email;

            await _userRepository.AddOrUpdateAsync(user);
            return NoContent();
        }

        /// <summary>
        /// Delete user
        /// </summary>
        /// <param name="id">User id</param>
        /// <returns></returns>
        [HttpDelete("{id:guid}")]
        public async Task<IActionResult> DeleteUserAsync(Guid id)
        {
            var user = await _userRepository.GetByIdAsync(id);
            if (user == null)
                return NotFound();

            await _userRepository.RemoveAsync(id);

            return NoContent();
        }
    }

}