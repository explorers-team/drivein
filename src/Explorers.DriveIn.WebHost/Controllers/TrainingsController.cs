﻿using Explorers.DriveIn.Abstractions;
using Explorers.DriveIn.Core.Domain;
using Explorers.DriveIn.WebHost.Controllers.Auth;
using Explorers.DriveIn.WebHost.Models;
using Microsoft.AspNetCore.Mvc;

namespace Explorers.DriveIn.WebHost.Controllers
{
    /// <summary>
    /// Trainings
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    [DriveInAuthorize]
    public class TrainingsController : Controller
    {
        //Models context
        private readonly IRepository<Training> _trainingsRepository;
        private readonly IRepository<User> _usersRepository;

        /// <summary>
        /// Trainings controller constructor
        /// </summary>
        /// <param name="trainingsRepository"></param>
        public TrainingsController(IRepository<Training> trainingsRepository, IRepository<User> usersRepository)
        {
            _trainingsRepository = trainingsRepository;
            _usersRepository = usersRepository;
        }

        /// <summary>
        /// Get all trainings
        /// </summary>
        /// <returns>Trainings collection</returns>
        [HttpGet]
        public async Task<ActionResult<List<Training>>> GetTrainingsAsync()
        {
            var trainings = (await _trainingsRepository
                .GetAllAsync())
                .OrderByDescending(tr => tr.DateBegin);
            return Ok(trainings);
        }

        /// <summary>
        /// Get training by Id  
        /// </summary>
        /// <returns>Training</returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<Training>> GetTrainingAsync(Guid id)
        {
            var training = await _trainingsRepository.GetByIdAsync(id);
            if (training == null)
                return NotFound();
            else
                return Ok(training);
        }

        /// <summary>
        /// Get training by user Id  
        /// </summary>
        /// <returns>Trainings</returns>
        [Route("by-user-id/{id:guid}")]
        [HttpGet]
        public async Task<ActionResult<List<Training>>> GetTrainingsByUserId(Guid userId)
        {
            var trainings = await _trainingsRepository.GetAllAsync();

            var userTrainings = trainings.Where(t => t.UserId == userId);

            if (userTrainings == null)
                return NotFound();
            else
                return Ok(userTrainings);
        }

        /// <summary>
        /// Create training
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> CreateTrainingAsync(CreateOrEditTrainingRequest request)
        {
            var user = await _usersRepository.GetByIdAsync(HttpContext.GetUserId());
            if (user == null)
                return BadRequest("User not found");

            Training training = new Training
            {
                UserId = user.Id,
                DateBegin = request.DateBegin,
                DateEnd = request.DateEnd,
                Distance = request.Distance,
                Comment = request.Comment
            };

            await _trainingsRepository.AddAsync(training);

            string trainingAsyncName = nameof(GetTrainingAsync);
            return CreatedAtAction(trainingAsyncName, new { id = training.Id }, training);
        }

        /// <summary>
        /// Update training
        /// </summary>
        /// <param name="id">Training id</param>
        /// <param name="request">Update request</param>
        /// <returns></returns>
        [HttpPut("{id:guid}")]
        public async Task<IActionResult> EditTrainingAsync(Guid id, CreateOrEditTrainingRequest request)
        {
            var training = await _trainingsRepository.GetByIdAsync(id);

            if (training == null)
                return BadRequest("Training not found");

            var user = await _usersRepository.GetByIdAsync(HttpContext.GetUserId());
            if (user == null)
                return BadRequest("User not found");

            Training updTraining = training;
            updTraining.UserId = user.Id;
            updTraining.DateBegin = request.DateBegin;
            updTraining.DateEnd = request.DateEnd;
            updTraining.Distance = request.Distance;
            updTraining.Comment = request.Comment;

            await _trainingsRepository.AddOrUpdateAsync(updTraining);

            var trainingAsyncName = nameof(GetTrainingAsync);
            return CreatedAtAction(trainingAsyncName, new { id = updTraining.Id }, null);
        }

        /// <summary>
        /// Delete training
        /// </summary>
        /// <param name="id">Training id</param>
        /// <returns></returns>
        [HttpDelete("{id:guid}")]
        public async Task<IActionResult> DeleteTrainingAsync(Guid id)
        {
            var training = await _trainingsRepository.GetByIdAsync(id);
            if (training == null)
                return NotFound();

            await _trainingsRepository.RemoveAsync(id);

            return NoContent();
        }
    }
}