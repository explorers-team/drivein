﻿namespace Explorers.DriveIn.WebHost.Chat.Model;

/// <summary>
/// Message to sending to chat on client.
/// </summary>
public class ChatMessage
{
    /// <summary>
    /// User name.
    /// </summary>
    public string User { get; set; }

    /// <summary>
    /// Message.
    /// </summary>
    public string Message { get; set; }
}