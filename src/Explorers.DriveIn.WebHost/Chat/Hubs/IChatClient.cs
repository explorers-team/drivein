﻿using Explorers.DriveIn.WebHost.Chat.Model;

namespace Explorers.DriveIn.WebHost.Chat.Hubs;

/// <summary>
/// Interface on client side.
/// </summary>
public interface IChatClient
{
    /// <summary>
    /// Receive message method to call client side.
    /// </summary>
    /// <param name="message">Message.</param>
    /// <returns></returns>
    Task ReceiveMessage(ChatMessage message);
}