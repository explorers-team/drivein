﻿using Explorers.DriveIn.WebHost.Chat.Hubs;
using Explorers.DriveIn.WebHost.Chat.Model;
using Microsoft.AspNetCore.SignalR;

namespace Explorers.DriveIn.WebHost.Chat;

/// <summary>
/// Chat hab to sending messages to listeners.
/// </summary>
public class ChatHub : Hub<IChatClient>
{
    /// <summary>
    /// Send message to all listeners (clients).
    /// </summary>
    /// <param name="message"></param>
    public async Task SendMessage(ChatMessage message)
    {
        await Clients.All.ReceiveMessage(message);
    }
}