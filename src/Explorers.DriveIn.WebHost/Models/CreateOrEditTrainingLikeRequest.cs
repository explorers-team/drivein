﻿using Explorers.DriveIn.Core.Domain;

namespace Explorers.DriveIn.WebHost.Models
{
    public class CreateOrEditTrainingLikeRequest
    {
        /// <summary>
        /// Training
        /// </summary>
        public Guid TrainingId { get; set; }

        /// <summary>
        /// Liking user
        /// </summary>
        public Guid UserId { get; set; }

        /// <summary>
        /// Date like
        /// </summary>
        public DateTime DateLike { get; set; }
    }
}
