﻿using Microsoft.AspNetCore.Mvc;

namespace Explorers.DriveIn.WebHost.Models
{
    public class CreateOrEditTrainingRequest
    {
        /// <summary>
        /// Training begin date
        /// </summary>
        public DateTime DateBegin { get; set; }


        /// <summary>
        /// Training end date
        /// </summary>
        public DateTime DateEnd { get; set; }

        /// <summary>
        /// Distance
        /// </summary>
        public int Distance { get; set; }

        /// <summary>
        /// Comment
        /// </summary>
        public string Comment { get; set; }
    }
}
