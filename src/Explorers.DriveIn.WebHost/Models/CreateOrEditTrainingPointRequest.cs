﻿namespace Explorers.DriveIn.WebHost.Models
{
    public class CreateOrEditTrainingPointRequest
    {
        /// <summary>
        /// Training id
        /// </summary>
        public Guid TrainingId { get; set; }

        /// <summary>
        /// Point latitude
        /// </summary>
        public double Latitude { get; set; }

        /// <summary>
        /// Ping longitude
        /// </summary>
        public double Longitude { get; set; }

        /// <summary>
        /// Date point
        /// </summary>
        public DateTime DatePoint { get; set; }
    }
}
