﻿using Explorers.DriveIn.Core.Domain;

namespace Explorers.DriveIn.WebHost.Models
{
    public class CreateOfEditUserFollowerRequest
    {
        /// <summary>
        /// User
        /// </summary>
        public User User { get; set; }

        /// <summary>
        /// Follower
        /// </summary>
        public User Follower { get; set; }
    }
}
