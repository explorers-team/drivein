﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Explorers.DriveIn.NotificationService.Core.Domain;

namespace Explorers.DriveIn.NotificationService.Core.Abstractions
{
    public interface INotificationService
    {
        Task NotifyUserAboutFollower(UserFollower follower);
    }
}
