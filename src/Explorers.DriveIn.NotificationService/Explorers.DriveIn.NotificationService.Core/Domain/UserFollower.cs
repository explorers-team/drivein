﻿namespace Explorers.DriveIn.NotificationService.Core.Domain
{
    /// <summary>
    /// 
    /// </summary>
    public class UserFollower : BaseEntity
    {
        /// <summary>
        /// User
        /// </summary>
        public User User { get; set; }

        /// <summary>
        /// Follower
        /// </summary>
        public User Follower { get; set; }

        /// <summary>
        /// Date follow for user
        /// </summary>
        public DateTime DateFollow { get; set; }
    }
}
