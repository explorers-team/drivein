﻿using System.Text;
using Explorers.DriveIn.NotificationService.Core.Domain;
using Explorers.DriveIn.NotificationService.Services;
using Microsoft.AspNetCore.Connections;
using Newtonsoft.Json;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace Explorers.DriveIn.NotificationService.WebHost.Services
{
    public class MessageRecieveService : BackgroundService
    {
        private readonly IServiceProvider _serviceProvider;
        private readonly ConnectionFactory _connectionFactory;
        private readonly IConnection _connection;
        private readonly IModel _channel;
        private readonly IConfiguration _configuration;

        public MessageRecieveService(IServiceProvider serviceProvider, IConfiguration configuration)
        {
            _configuration = configuration;
            _serviceProvider = serviceProvider;

            _connectionFactory = new ConnectionFactory() { HostName = _configuration["RabbitMQ:Host"] };

            _connection = _connectionFactory.CreateConnection();

            _channel = _connection.CreateModel();

            _channel.QueueDeclare(
                queue: "NotificationService",
                durable: false,
                exclusive: false,
                autoDelete: false,
                arguments: null);
        }

        protected override Task ExecuteAsync(CancellationToken stoppingToken)
        {
            if (stoppingToken.IsCancellationRequested)
            {
                _channel.Dispose();
                _connection.Dispose();
                return Task.CompletedTask;
            }

            var consumer = new EventingBasicConsumer(_channel);

            consumer.Received += (model, ea) =>
            {
                var body = ea.Body.ToArray();

                var message = Encoding.UTF8.GetString(body);
                Console.WriteLine(message);

                var follower = JsonConvert.DeserializeObject<UserFollower>(message);

                if (follower != null)
                {
                    EmailNotificationService notificationService = new EmailNotificationService();
                    notificationService.NotifyUserAboutFollower(follower);
                }
            };

            _channel.BasicConsume(queue: "NotificationService", autoAck: true, consumer: consumer);

            return Task.CompletedTask;
        }
    }
}
