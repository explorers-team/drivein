﻿using Explorers.DriveIn.NotificationService.Core.Abstractions;
using Explorers.DriveIn.NotificationService.Core.Domain;

namespace Explorers.DriveIn.NotificationService.Services
{
    public class EmailNotificationService : INotificationService
    {
        public Task NotifyUserAboutFollower(UserFollower follower)
        {
            //Здесь будет логика отправки уведомления на Email
            return Task.CompletedTask;
        }
    }
}