﻿using AutoFixture;
using AutoFixture.AutoMoq;
using Explorers.DriveIn.Abstractions;
using Explorers.DriveIn.Core.Domain;
using Explorers.DriveIn.WebHost.Controllers;
using Explorers.DriveIn.WebHost.Models;
using Microsoft.AspNetCore.Mvc;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace Explorers.DriveIn.UnitTests.WebHost.Controllers.UsersController
{
    public class EditUserAsyncTests
    {
        private readonly Mock<IRepository<User>> _usersRepositoryMock;
        private readonly DriveIn.WebHost.Controllers.UsersController _usersController;

        public EditUserAsyncTests()
        {
            var fixture = new Fixture().Customize(new AutoMoqCustomization());
            _usersRepositoryMock = fixture.Freeze<Mock<IRepository<User>>>();
            _usersController = fixture.Build<DriveIn.WebHost.Controllers.UsersController>().OmitAutoProperties().Create();
        }

        [Fact]
        public async void EditUserAsync_NotExistedUser_NotFound()
        {
            //Arrange
            User user = new UserBuilder()
                .WhithCreateBaseUser()
                .Build();

            _usersRepositoryMock.Setup(repo => repo.GetByIdAsync(user.Id))
                .ReturnsAsync(user);

            CreateOrEditUserRequest request = new CreateOrEditUserRequest()
            {
                FirstName = user.FirstName,
                SecondName = user.SecondName,
                Email = user.Email,
                PhoneNumber = user.PhoneNumber
            };
            Guid userId = Guid.Parse("8f5dcd63-0f4a-4e4d-9e9b-a7ebbe5bd8d7");

            //Act
            var result = await _usersController.EditUserAsync(userId, request);

            //Assert
            Assert.IsType<NotFoundResult>(result);
        }

        [Fact]
        public async void EditUserAsync_UpdateUserRequest_UserUpdated()
        {
            //Arrange
            User user = new UserBuilder()
                .WhithCreateBaseUser()
                .Build();

            var usersAdded = new List<User>();
            _usersRepositoryMock.Setup(i => i.AddOrUpdateAsync(It.IsAny<User>()))
                .Callback((User u) => { user = u; } );
            _usersRepositoryMock.Setup(repo => repo.GetByIdAsync(user.Id))
                .ReturnsAsync(user);

            CreateOrEditUserRequest request = new CreateOrEditUserRequest()
            {
                FirstName = "Abraham",
                SecondName = "Lincoln",
                Email = user.Email,
                PhoneNumber = user.PhoneNumber
            };

            //Act
            var result = await _usersController.EditUserAsync(user.Id, request);

            //Assert
            var redirectToActionResult = Assert.IsType<NoContentResult>(result);
            _usersRepositoryMock.Verify(r => r.AddOrUpdateAsync(user));
            Assert.Equal("Abraham", user.FirstName);
        }
    }
}
