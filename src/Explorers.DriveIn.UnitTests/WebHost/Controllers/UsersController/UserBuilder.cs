﻿using Explorers.DriveIn.Abstractions;
using Explorers.DriveIn.Core.Domain;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;

namespace Explorers.DriveIn.UnitTests.WebHost.Controllers.UsersController
{
    public class UserBuilder
    {
        private User _user;

        public UserBuilder()
        {

        }

        public UserBuilder WhithCreateBaseUser()
        {
            _user = new User
            {
                Id = Guid.Parse("00f2f2f5-edef-431e-9763-d8b1d348917c"),
                FirstName = "Ivan",
                SecondName = "Grozniy",
                PhoneNumber = "79990000102",
                Email = "ivan.grozniy@email.com",
                DateRegistration = new DateTime(2022, 08, 01)
            };
            return this;
        }

        public User Build()
        {
            if (_user == null)
                WhithCreateBaseUser();
            return _user;
        }
    }
}
