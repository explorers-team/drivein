﻿using AutoFixture;
using AutoFixture.AutoMoq;
using Explorers.DriveIn.Abstractions;
using Explorers.DriveIn.Core.Domain;
using Explorers.DriveIn.WebHost.Controllers;
using Explorers.DriveIn.WebHost.Models;
using Microsoft.AspNetCore.Mvc;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace Explorers.DriveIn.UnitTests.WebHost.Controllers.UsersController
{
    public class CreateUserAsyncTests
    {
        private readonly Mock<IRepository<User>> _usersRepositoryMock;
        private readonly DriveIn.WebHost.Controllers.UsersController _usersController;

        public CreateUserAsyncTests()
        {
            var fixture = new Fixture().Customize(new AutoMoqCustomization());
            _usersRepositoryMock = fixture.Freeze<Mock<IRepository<User>>>();
            _usersController = fixture.Build<DriveIn.WebHost.Controllers.UsersController>().OmitAutoProperties().Create();
        }

        /// <summary>
        /// <see cref="DriveIn.WebHost.Controllers.UsersController.CreateUserAsync"/> should return CreatedAtActionResult, 
        /// and user entity was added to repository.
        /// </summary>
        [Fact]
        public async void CreateUserAsync_UserModel_UserCreated()
        {
            //Arrange
            User user = new UserBuilder()
                .WhithCreateBaseUser()
                .Build();

            var usersAdded = new List<User>();
            _usersRepositoryMock.Setup(i => i.AddAsync(It.IsAny<User>()))
                .Callback((User user) => usersAdded.Add(user));

            CreateOrEditUserRequest request = new CreateOrEditUserRequest()
            {
                FirstName = user.FirstName,
                SecondName = user.SecondName,
                Email = user.Email,
                PhoneNumber = user.PhoneNumber
            };

            //Act
            var result = await _usersController.CreateUserAsync(request);

            //Assert
            var redirectToActionResult = Assert.IsType<CreatedAtActionResult>(result);
            Assert.Equal("GetUserAsync", redirectToActionResult.ActionName);
            Assert.Single(usersAdded);
        }

        /// <summary>
        /// <see cref="DriveIn.WebHost.Controllers.UsersController.CreateUserAsync"/> should return BadRequestObjectResult
        /// if user already exist.
        /// </summary>
        [Fact]
        public async void CreateUserAsync_ExistUserModel_BadRequest()
        {
            //Arrange
            User user = new UserBuilder()
                .WhithCreateBaseUser()
                .Build();

            var usersAdded = new List<User>();
            _usersRepositoryMock.Setup(i => i.AddAsync(It.IsAny<User>()))
                .Callback((User user) => usersAdded.Add(user));
            _usersRepositoryMock.Setup(repo => repo.GetAllAsync())
                .ReturnsAsync(new List<User> { user });

            CreateOrEditUserRequest request = new CreateOrEditUserRequest()
            {
                FirstName = user.FirstName,
                SecondName = user.SecondName,
                Email = user.Email,
                PhoneNumber = user.PhoneNumber
            };

            //Act
            var result = await _usersController.CreateUserAsync(request);

            //Assert
            var redirectToActionResult = Assert.IsType<BadRequestObjectResult>(result);
        }
    }
}
