﻿using Explorers.DriveIn.WebHost.Models;
using Explorers.DriveIn.WebHost.ValidationRules;
using FluentValidation;
using FluentValidation.TestHelper;
using Xunit;

namespace Explorers.DriveIn.UnitTests.WebHost.ValidationRules
{
    public class CreateOrEditUserRequestValudatorTests
    {
        private readonly CreateOrEditUserRequestValudator _validator;

        public CreateOrEditUserRequestValudatorTests()
        {
            _validator = new CreateOrEditUserRequestValudator();
        }

        [Fact]
        public void SendRequestForCreateUser_CreateOrEditUserRequest_ValidationError()
        {
            //Arrange
            CreateOrEditUserRequest request = new CreateOrEditUserRequest
            {
                FirstName = "U",
                SecondName = "Smith",
                Email = "email@mail.mi",
                PhoneNumber = "+799988877S12s",
                Password = "Dasc12fkod!"
            };

            //Act
            var result = _validator.TestValidate(request);

            //Assert
            result.ShouldHaveValidationErrorFor(user => user.FirstName);
            result.ShouldNotHaveValidationErrorFor(user => user.SecondName);
            result.ShouldNotHaveValidationErrorFor(user => user.Email);
            result.ShouldHaveValidationErrorFor(user => user.PhoneNumber);
            result.ShouldNotHaveValidationErrorFor(user => user.Password);
        }
    }
}
