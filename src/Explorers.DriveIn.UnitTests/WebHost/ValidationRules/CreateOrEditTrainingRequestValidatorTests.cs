﻿using Explorers.DriveIn.WebHost.Models;
using Explorers.DriveIn.WebHost.ValidationRules;
using FluentValidation;
using FluentValidation.TestHelper;
using Xunit;

namespace Explorers.DriveIn.UnitTests.WebHost.ValidationRules
{
    public class CreateOrEditTrainingRequestValidatorTests
    {
        private readonly CreateOrEditTrainingRequestValidator _validator;

        public CreateOrEditTrainingRequestValidatorTests()
        {
            _validator = new CreateOrEditTrainingRequestValidator();
        }

        [Fact]
        public void SendRequestForCreateUser_CreateOrEditUserRequest_ValidationError()
        {
            //Arrange
            CreateOrEditTrainingRequest request = new CreateOrEditTrainingRequest
            {
                DateBegin = System.DateTime.Now.AddHours(-1),
                DateEnd = System.DateTime.Now,
                Distance = 0
            };

            //Act
            var result = _validator.TestValidate(request);

            //Assert
            result.ShouldNotHaveValidationErrorFor(tr => tr.DateBegin);
            result.ShouldNotHaveValidationErrorFor(tr => tr.DateBegin);
            result.ShouldHaveValidationErrorFor(tr => tr.Distance);
        }
    }
}
