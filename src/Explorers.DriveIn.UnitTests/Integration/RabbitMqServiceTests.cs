﻿using AutoFixture.AutoMoq;
using AutoFixture;
using Explorers.DriveIn.Integration;
using Microsoft.Extensions.Configuration;
using Moq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Explorers.DriveIn.Core.Domain;

namespace Explorers.DriveIn.UnitTests.Integration
{
    public class RabbitMqServiceTests
    {
        private readonly Microsoft.Extensions.Configuration.ConfigurationManager configuration;
        public RabbitMqServiceTests()
        {
            var fixture = new Fixture().Customize(new AutoMoqCustomization());
            configuration = fixture.Build<Microsoft.Extensions.Configuration.ConfigurationManager>().
                OmitAutoProperties().
                Create();
        }

        [Fact]
        public void SendMessage_UserFollower_MessageSended()
        {
            //Arrange
            configuration["RabbitMQ:Host"] = "localhost";
            RabbitMqService rabbitMqService = new RabbitMqService(configuration);
            UserFollower userFollower = new UserFollower()
            {
                Id = Guid.NewGuid(),
                User = new User() { FirstName = "Петров" },
                Follower = new User() { FirstName = "Иванов" },
                DateFollow = DateTime.Now
            };


            //Act
            rabbitMqService.SendMessage(userFollower, "NotificationService");

            //Assert
            
        }
    }
}
