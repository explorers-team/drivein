﻿namespace Explorers.DriveIn.Core.Domain
{
    /// <summary>
    /// User model
    /// </summary>
    public class User : BaseEntity
    {
        /// <summary>
        /// User first name
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// User second name
        /// </summary>
        public string SecondName { get; set; }

        /// <summary>
        /// User phone number
        /// </summary>
        public string PhoneNumber { get; set; }

        /// <summary>
        /// User email
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// User date registration
        /// </summary>
        public DateTime DateRegistration { get; set; }

        /// <summary>
        /// Calculated password hash.
        /// </summary>
        public string PasswordHash { get; set; }

        /// <summary>
        /// Salt to calculate <see cref="PasswordHash"/>.
        /// </summary>
        public string Salt { get; set; }

        /// <summary>
        /// Тренировки.
        /// </summary>
        public List<Training> Trainings { get; set; }
    }
}
