﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Explorers.DriveIn.Core.Domain
{
    /// <summary>
    /// 
    /// </summary>
    public class UserFollower : BaseEntity
    {
        /// <summary>
        /// User
        /// </summary>
        public User User { get; set; }

        /// <summary>
        /// Follower
        /// </summary>
        public User Follower { get; set; }

        /// <summary>
        /// Date follow for user
        /// </summary>
        public DateTime DateFollow { get; set; }
    }
}
