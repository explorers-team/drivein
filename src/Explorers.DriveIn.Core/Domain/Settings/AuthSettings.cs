﻿namespace Explorers.DriveIn.Core.Domain.Settings;

/// <summary>
/// Authentication/authorization settings.
/// </summary>
public class AuthSettings
{
    /// <summary>
    /// JWT symmetric encryption key.
    /// </summary>
    public string JwtSecret { get; set; }
}