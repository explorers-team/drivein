﻿namespace Explorers.DriveIn.Core.Domain
{
    /// <summary>
    /// Training information
    /// </summary>
    public class Training : BaseEntity
    {
        /// <summary>
        /// Идентификатор пользователя.
        /// </summary>
        public Guid UserId { get; set; }
        
        // /// <summary>
        // /// User
        // /// </summary>
        // public User User { get; set; }

        /// <summary>
        /// Training begin date
        /// </summary>
        public DateTime DateBegin { get; set; }


        /// <summary>
        /// Training end date
        /// </summary>
        public DateTime DateEnd { get; set; }

        /// <summary>
        /// Distance
        /// </summary>
        public int Distance { get; set; }

        /// <summary>
        /// Comment
        /// </summary>
        public string Comment { get; set; }
    }
}
