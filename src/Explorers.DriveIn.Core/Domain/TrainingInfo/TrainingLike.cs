﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Explorers.DriveIn.Core.Domain
{
    /// <summary>
    /// Training like
    /// </summary>
    public class TrainingLike : BaseEntity
    {
        /// <summary>
        /// Training
        /// </summary>
        public Training Training { get; set; }

        /// <summary>
        /// Liking user
        /// </summary>
        public User User { get; set; }

        /// <summary>
        /// Date like
        /// </summary>
        public DateTime DateLike { get; set; }
    }
}
