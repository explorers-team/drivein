﻿namespace Explorers.DriveIn.Core.Domain.Auth;

/// <summary>
/// Refresh token information.
/// </summary>
/// <remarks>
/// Used to generate JWT-token. 
/// </remarks>
public class RefreshToken : BaseEntity
{
    /// <summary>
    /// Token value string.
    /// </summary>
    public string TokenValue { get; set; }

    /// <summary>
    /// Date time where refresh token will be expired.
    /// </summary>
    public DateTimeOffset ExpiredAt { get; set; }

    /// <summary>
    /// Identifier of user who owns the token.
    /// </summary>
    public Guid UserId { get; set; }

    /// <summary>
    /// User information.
    /// </summary>
    public User User { get; set; }
}