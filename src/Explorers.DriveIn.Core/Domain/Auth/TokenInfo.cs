﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Explorers.DriveIn.Core.Domain.Auth
{
    /// <summary>
    /// Token info.
    /// </summary>
    public class TokenInfo
    {
        /// <summary>
        /// Name of user.
        /// </summary>
        public string UserName { get; }
        
        /// <summary>
        /// Access token. 
        /// </summary>
        public string AccessToken { get; }

        /// <summary>
        /// Refresh token. 
        /// </summary>
        public string RefreshToken { get; }

        public TokenInfo(string userName, string accessToken, string refreshToken)
        {
            UserName = userName;
            AccessToken = accessToken;
            RefreshToken = refreshToken;
        }
    }
}
