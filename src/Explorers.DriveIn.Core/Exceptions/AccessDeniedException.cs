﻿using System.Runtime.Serialization;

namespace Explorers.DriveIn.Core.Exceptions;

/// <summary>
/// Throw it on access denied event.
/// </summary>
public class AccessDeniedException : Exception
{
    
}