﻿using Explorers.DriveIn.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Explorers.DriveIn.Abstractions.Gateways
{
    public interface ITrainingPointsGateway
    {
        /// <summary>
        /// Getting all training points
        /// </summary>
        /// <returns>Training points collection</returns>
        Task<IEnumerable<TrainingPoint>> GetTrainingPointsAsync();

        /// <summary>
        /// Getting training point by id
        /// </summary>
        /// <returns>Training point</returns>
        Task<TrainingPoint> GetTrainingPointsByIdAsync(Guid id);

        /// <summary>
        /// Getting training points by training id
        /// </summary>
        /// <param name="trainingId">Training id</param>
        /// <returns>Training points collection by training id</returns>
        Task<IEnumerable<TrainingPoint>> GetTrainingPointsByTrainingIdAsync(Guid trainingId);

        /// <summary>
        /// Add or update training point
        /// </summary>
        /// <param name="point">Training point</param>
        Task AddOrUpdateTrainingPointAsync(TrainingPoint point);

        /// <summary>
        /// Delete training point by i
        /// </summary>
        /// <param name="id">Training point id</param>
        Task DeleteTrainingPointAsync(Guid id);
    }
}
