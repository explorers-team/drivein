﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Explorers.DriveIn.Abstractions.MessageQueue
{
    public interface INotificationMqService
    {
        /// <summary>
        /// Sending message to queue
        /// </summary>
        /// <param name="obj">Message object</param>
        void SendMessage(object obj);

        /// <summary>
        /// Sending message to queue
        /// </summary>
        /// <param name="message">Message body</param>
        void SendMessage(string message);
    }
}
