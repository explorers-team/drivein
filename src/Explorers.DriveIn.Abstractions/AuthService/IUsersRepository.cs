﻿using Explorers.DriveIn.Core.Domain;

namespace Explorers.DriveIn.Abstractions.AuthService;

/// <summary>
/// Repository for users.
/// </summary>
public interface IUsersRepository : IRepository<User>
{
    /// <summary>
    /// Get user.
    /// </summary>
    /// <param name="phoneNumber">Login of user.</param>
    /// <returns></returns>
    public Task<User?> GetUserByPhoneNumber(string phoneNumber);

    /// <summary>
    /// Get user.
    /// </summary>
    /// <param name="refreshToken">Active user refresh token.</param>
    /// <returns></returns>
    User GetUserByRefreshToken(string refreshToken);
}
