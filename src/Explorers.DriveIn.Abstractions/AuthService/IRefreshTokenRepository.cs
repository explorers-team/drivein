﻿using Explorers.DriveIn.Core.Domain.Auth;

namespace Explorers.DriveIn.Abstractions.AuthService
{
    /// <summary>
    /// Repository to work with user refresh-tokens.
    /// </summary>
    public interface IRefreshTokenRepository : IRepository<RefreshToken>
    {
        /// <summary>
        /// Get refresh-token by token value. 
        /// </summary>
        /// <param name="refreshToken">Refresh-token.</param>
        /// <returns></returns>
        RefreshToken GetByValue(string refreshToken);
    }
}