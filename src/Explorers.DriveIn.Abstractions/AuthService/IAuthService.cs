﻿using Explorers.DriveIn.Core.Domain.Auth;
using Explorers.DriveIn.Core.Exceptions;

namespace Explorers.DriveIn.Abstractions.AuthService;

public interface IAuthService
{
    /// <summary>
    /// Log in user.
    /// Generate {JWT-token, Refresh-token} info.
    /// </summary>
    /// <param name="login">Login of user.</param>
    /// <param name="password">Password of user.</param>
    /// <returns>JWT- and Refresh-tokens information.</returns>
    /// <exception cref="AccessDeniedException">Login or password invalid.</exception>
    Task<TokenInfo> Login(string login, string password);

    /// <summary>
    /// Generate new pair {JWT-token, Refresh-token} information.
    /// </summary>
    /// <param name="refreshTokenValue"></param>
    /// <returns></returns>
    Task<TokenInfo> RefreshJwtToken(string refreshTokenValue);

    /// <summary>
    /// Create password hash
    /// </summary>
    /// <param name="password">Password</param>
    /// <param name="salt">Salt</param>
    /// <returns></returns>
    string CreatePasswordHash(string password, string salt);

    /// <summary>
    /// Generate salt
    /// </summary>
    /// <returns>Password salt</returns>
    string GenerateSalt();
}