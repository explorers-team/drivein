﻿using Explorers.DriveIn.Abstractions.Gateways;
using Explorers.DriveIn.Core.Domain;
using Explorers.DriveIn.TrainingPointsService.WebHost.Models;
using Newtonsoft.Json;

namespace Explorers.DriveIn.Integration
{
    public class TrainingPointsGateway : ITrainingPointsGateway
    {
        private readonly HttpClient _httpClient;

        public TrainingPointsGateway(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        /// <inheritdoc />
        public async Task AddOrUpdateTrainingPointAsync(TrainingPoint point)
        {
            var trainingPointsMessage = await _httpClient.GetAsync($"api/v1/points/trainingpoints/{point.Id}");

            if(trainingPointsMessage.IsSuccessStatusCode)
            {
                var updateRequest = new CreateOrEditPointRequest
                {
                    TrainingId = point.TrainingId,
                    Latitude = point.Latitude,
                    Longitude = point.Longitude,
                    DatePoint = point.DatePoint
                };
                var updateResponse = await _httpClient.PutAsync($"api/v1/points/trainingpoints/{point.Id}",
                    new StringContent(JsonConvert.SerializeObject(updateRequest)));

                updateResponse.EnsureSuccessStatusCode();
            }
            else
            {
                var createRequest = new CreateOrEditPointRequest
                {
                    TrainingId = point.TrainingId,
                    Latitude = point.Latitude,
                    Longitude = point.Longitude,
                    DatePoint = point.DatePoint
                };
                var createResponse = await _httpClient.PostAsync("api/v1/points/trainingpoints",
                    new StringContent(JsonConvert.SerializeObject(createRequest)));

                createResponse.EnsureSuccessStatusCode();
            }
        }

        /// <inheritdoc />
        public async Task<IEnumerable<TrainingPoint>> GetTrainingPointsAsync()
        {
            var trainingPointsMessage = await _httpClient.GetAsync("api/v1/points/trainingpoints");

            trainingPointsMessage.EnsureSuccessStatusCode();

            var trainingPoints = JsonConvert.DeserializeObject<IEnumerable<TrainingPoint>>(
                await trainingPointsMessage.Content.ReadAsStringAsync());

            return trainingPoints;
        }

        /// <inheritdoc />
        public async Task<TrainingPoint> GetTrainingPointsByIdAsync(Guid id)
        {
            var trainingPointsMessage = await _httpClient.GetAsync($"api/v1/points/trainingpoints/{id}");

            trainingPointsMessage.EnsureSuccessStatusCode();

            var trainingPoint = JsonConvert.DeserializeObject<TrainingPoint>(
                await trainingPointsMessage.Content.ReadAsStringAsync());

            return trainingPoint;
        }

        /// <inheritdoc />
        public async Task<IEnumerable<TrainingPoint>> GetTrainingPointsByTrainingIdAsync(Guid trainingId)
        {
            var trainingPointsMessage = await _httpClient.GetAsync($"api/v1/points/trainingpoints/by-training-id/{trainingId}");

            trainingPointsMessage.EnsureSuccessStatusCode();

            var trainingPoints = JsonConvert.DeserializeObject<IEnumerable<TrainingPoint>>(
                await trainingPointsMessage.Content.ReadAsStringAsync());

            return trainingPoints;
        }

        /// <inheritdoc />
        public async Task DeleteTrainingPointAsync(Guid id)
        {
            var deleteResponse = await _httpClient.DeleteAsync($"api/v1/points/trainingpoints/{id}");

            deleteResponse.EnsureSuccessStatusCode();
        }
    }
}