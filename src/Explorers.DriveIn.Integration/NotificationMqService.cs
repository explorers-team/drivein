﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Explorers.DriveIn.Abstractions.MessageQueue;

namespace Explorers.DriveIn.Integration
{
    public class NotificationMqService : INotificationMqService
    {
        private readonly RabbitMqService _rabbitMqQueueService;

        public NotificationMqService(IConfiguration configuration)
        {
            _rabbitMqQueueService = new RabbitMqService(configuration);
        }

        /// <inheritdoc />
        public void SendMessage(object obj)
        {
            _rabbitMqQueueService.SendMessage(obj, "NotificationService");
        }

        /// <inheritdoc />
        public void SendMessage(string message)
        {
            _rabbitMqQueueService.SendMessage(message, "NotificationService");
        }
    }
}
